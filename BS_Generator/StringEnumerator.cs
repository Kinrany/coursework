﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace BS_Generator
{
	class StringEnumerator: IEnumerator<string>
	{
		public StringEnumerator(TextReader textReader) {
			this.textReader = textReader;
			current = new StringBuilder();
		}

		public string Current {
			get { return current.ToString(); }
		}

		public void Dispose() { }

		object System.Collections.IEnumerator.Current {
			get { return current.ToString(); }
		}

		public bool MoveNext() {
			current.Clear();
			int chr = (int)' ';
			while (isSeparator(chr)) {
				chr = textReader.Read();
			}
			while (!isSeparator(chr) && chr != -1) {
				current.Append((char)chr);
				chr = textReader.Read();
			}
			return (current.Length != 0);
		}

		public void Reset() {
			throw new InvalidOperationException("Не поддерживается.");
		}

		private bool isSeparator(int chr) {
			return Char.IsSeparator((char)chr) || Char.IsControl((char)chr);
		}

		private TextReader textReader;
		private StringBuilder current;
	}
}
