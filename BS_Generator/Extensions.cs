﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Extensions {
	interface ICountable<T>
	{
		int Count { get; }

		T this[int index] { get; }
	}

	static class ExternRandom {
		public static T Pick<T>(this Random random, ICountable<T> list) {
			if (list == null || list.Count == 0) return default(T);

			int pickedIndex = random.Next(list.Count);
			return list[pickedIndex];
		}
	}

	static class ExternStringArray {
		public static string[] Cycle(this string[] array, string word) {
			int length = array.Length;
			string[] newArray = new string[length];
			for (int i = 0; i < length - 1; ++i) {
				newArray[i] = array[i+1];
			}
			newArray[length - 1] = word;
			return newArray;
		}

		public static string CustomToString(this string[] array) {
			StringBuilder stringBuilder = new StringBuilder(array[0]);
			for (int i = 1; i < array.Length; ++i) {
				stringBuilder.AppendFormat(" {0}", array[i]);
			}
			return stringBuilder.ToString();
		}

		public static void Fill(this string[] array, string str) {
			for (int i = 0; i < array.Length; ++i) {
				array[i] = str;
			}
		}
	}

	static class ExternTextReader
	{
		public static void RemoveCharacter(this TextReader textReader, char character) {
			int chr = textReader.Read();
			if (chr != (int)character)
				throw new InvalidDataException(
					String.Format("Встречен символ '{0}'({1}) вместо '{2}'", 
						(char)chr, chr, character));
		}

		public static void RemoveString(this TextReader textReader, string str) {
			foreach (char character in str) {
				textReader.RemoveCharacter(character);
			}
		}
	}

	static class ExternString
	{
		public static string ToJSON(this string str) {
			StringBuilder stringBuilder = new StringBuilder(str);
			stringBuilder.Replace(@"\", @"\\");
			stringBuilder.Replace(@"[", @"\[");
			stringBuilder.Replace(@"]", @"\]");
			stringBuilder.Replace(@",", @"\,");
			stringBuilder.Replace(@"{", @"\{");
			stringBuilder.Replace(@"}", @"\}");
			stringBuilder.Replace(@":", @"\:");
			return stringBuilder.ToString();
		}

		public static string FromJSON(this string str) {
			StringBuilder stringBuilder = new StringBuilder(str);
			stringBuilder.Replace(@"\[", @"[");
			stringBuilder.Replace(@"\]", @"]");
			stringBuilder.Replace(@"\,", @",");
			stringBuilder.Replace(@"\{", @"{");
			stringBuilder.Replace(@"\}", @"}");
			stringBuilder.Replace(@"\:", @":");
			stringBuilder.Replace(@"\\", @"\");
			return stringBuilder.ToString();
		}
	}

	static class ExternStringBuilder
	{
		public static StringBuilder ToJSON(this StringBuilder stringBuilder) {
			stringBuilder.Replace(@"\", @"\\");
			stringBuilder.Replace(@"[", @"\[");
			stringBuilder.Replace(@"]", @"\]");
			stringBuilder.Replace(@",", @"\,");
			stringBuilder.Replace(@"{", @"\{");
			stringBuilder.Replace(@"}", @"\}");
			stringBuilder.Replace(@":", @"\:");
			return stringBuilder;
		}

		public static StringBuilder FromJSON(this StringBuilder stringBuilder) {
			stringBuilder.Replace(@"\[", @"[");
			stringBuilder.Replace(@"\]", @"]");
			stringBuilder.Replace(@"\,", @",");
			stringBuilder.Replace(@"\{", @"{");
			stringBuilder.Replace(@"\}", @"}");
			stringBuilder.Replace(@"\:", @":");
			stringBuilder.Replace(@"\\", @"\");
			return stringBuilder;
		}
	}
}
