﻿using System;
using System.Collections.Generic;
using System.IO;

using Extensions;
using JSONStream;

namespace BS_Generator {
	class Program {
		static void Main(string[] args) {

			Queue<string> queue = new Queue<string>(args);

			string mode = queue.Dequeue();
			switch (mode) {
				case "learn":
					Learn(queue);
					break;
				case "gen":
					Generate(queue);
					break;
				case "help":
					DisplayHelp();
					break;
				default:
					Console.WriteLine("Отсутствует режим с названием " + mode + ".");
					DisplayHelp();
					break;
			}

			Console.WriteLine();
			Console.WriteLine("Для выхода нажмите любую клавишу.");
			Console.ReadKey();
		}

		static void DisplayHelp() {
			Console.WriteLine(@"Программа работает в двух режимах: обучения и генерации.
В режиме обучения программа составляет модель языка и сохраняет в файл. 
Формат: learn <путь для сохранения модели> <первый файл> [второй файл] ...
Пример: learn model.txt file1.txt file2.txt file3.txt

В режиме генерации программа создаёт текст по данной модели языка. 
Формат: gen <путь для загрузки модели> [количество слов в генерируемом тексте] [файл для сохранения сгенерированного текста] [, первые слова генерируемого текста]
Пример: gen model.txt 500 output.txt , Hello world
");
		}

		static void Learn(Queue<string> args) {
			if (args.Count == 0) {
				Console.WriteLine("Не указан путь для сохранения языковой модели.");
				return;
			}
			string modelFileName = args.Dequeue();

			if (args.Count == 0) {
				Console.WriteLine("Не задано ни одного текста для обучения.");
				return;
			}

			Generator<LazySuffix> generator = new Generator<LazySuffix>();

			foreach (string filePath in args) {
				try {
					TextReader reader = new StreamReader(filePath);
					generator.Learn(reader);
					reader.Close();
				}
				catch (Exception e) {
					Console.WriteLine("Возникла ошибка при открытии одного из файлов: " + e.Message);
					return;
				}
			}

			JSONWriter jsonWriter = new JSONWriter(new StreamWriter(modelFileName));
			generator.Save(jsonWriter);
			jsonWriter.Close();
		}

		static void Generate(Queue<string> args) {
			// Файл языковой модели.
			if (args.Count == 0) {
				Console.WriteLine("Не указан путь для загрузки языковой модели.");
				return;
			}
			string modelFileName = args.Dequeue();

			// Размер генерируемого текста.
			int textLength;
			if (!int.TryParse(args.Peek(), out textLength)) {
				textLength = 100;
				Console.WriteLine("Не указан размер генерируемого текста. Значение по умолчанию - {0} слов.", textLength);
			}
			else {
				if (textLength <= 0) {
					Console.WriteLine("Размер генерируемого текста должен быть положительным.");
					return;
				} 
				args.Dequeue();
			}

			// Файл генерируемого текста.
			TextWriter resultFileStream = Console.Out;
			if (args.Count > 0 && args.Peek() != ",") {
				string resultFileName = args.Dequeue();
				try {
					resultFileStream = new StreamWriter(resultFileName);
				}
				catch {
					Console.WriteLine("Не удалось открыть файл {0} для записи.", resultFileName);
					return;
				}
			}

			// Первые слова генерируемого текста.
			string[] firstWords = new string[Generator.PREFIX_LENGTH];
			firstWords.Fill("");
			if (args.Count > 0 && args.Dequeue() == ",") {
				while (args.Count > firstWords.Length) {
					resultFileStream.Write(args.Dequeue() + " ");
				}
				args.CopyTo(firstWords, firstWords.Length - args.Count);
			}

			JSONReader reader = new JSONReader(new StreamReader(modelFileName));
			Generator<SlimSuffix> generator = Generator.Load(reader);
			reader.Close();

			generator.Generate(resultFileStream, firstWords, textLength);
			resultFileStream.WriteLine();
			if (resultFileStream != (TextWriter)Console.Out) {
				resultFileStream.Close();
			}
		}
	}
}
