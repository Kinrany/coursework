﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;

using Extensions;

using JSONStream;

namespace BS_Generator {

	class PrefixEqualityComparer<T>: IComparer<T>
		where T: ICollection<string>
	{
		public int Compare(T x, T y) {
			IEnumerator<string> enumeratorX = x.GetEnumerator();
			IEnumerator<string> enumeratorY = y.GetEnumerator();

			bool successX = enumeratorX.MoveNext();
			bool successY = enumeratorY.MoveNext();

			while (successX && successY) {
				string currentX = enumeratorX.Current;
				string currentY = enumeratorY.Current;

				if (currentX == currentY) {
					successX = enumeratorX.MoveNext();
					successY = enumeratorY.MoveNext();
				}
				else {
					return string.Compare(currentX, currentY);
				}
			}

			if (successX) {
				return string.Compare("ab", "a");
			}
			else if (successY) {
				return string.Compare("a", "ab");
			}
			else return 0;
		}
	}

	class LanguageModel
	{
		public static LanguageModel<SlimSuffix> Load(JSONReader stream) {
			LanguageModel<SlimSuffix> languageModel = new LanguageModel<SlimSuffix>();

			stream.CurvedOpen();
			stream.SkipWhitespace();

			var pair = loadKeyValuePair(stream);
			languageModel.Add(pair.Key, pair.Value);

			while (stream.Peek() == (char)',') {
				stream.Comma();
				stream.SkipWhitespace();
				pair = loadKeyValuePair(stream);
				languageModel.Add(pair.Key, pair.Value);
			}

			stream.SkipWhitespace();
			stream.CurvedClose();

			return languageModel;
		}

		private static KeyValuePair<string[], SlimSuffix> loadKeyValuePair(JSONReader stream) {
			string[] array = new string[Generator.PREFIX_LENGTH];

			stream.SquareOpen();
			array[0] = stream.String();
			for (int i = 1; i < array.Length; ++i) {
				stream.Comma();
				stream.Space();
				array[i] = stream.String();
			}
			stream.SquareClose();

			stream.Colon();

			SlimSuffix slimSuffix = SlimSuffix.Load(stream);

			return new KeyValuePair<string[], SlimSuffix>(array, slimSuffix);
		}
	}

	class LanguageModel<TSuffix>
		where TSuffix: ISuffixModel, new()
	{
		public LanguageModel() {
			dictionary = new SortedDictionary<string[], TSuffix>(new PrefixEqualityComparer<string[]>());
		}

		public void Add(string[] prefix, string word) {
			if (!dictionary.ContainsKey(prefix)) {
				dictionary[prefix] = new TSuffix();
			}
			dictionary[prefix].Add(word);
		}

		public void Add(string[] prefix, TSuffix suffix) {
			dictionary[prefix] = suffix;
		}

		public TSuffix this[string[] prefix] {
			get {
				if (!dictionary.ContainsKey(prefix)) {
					return default(TSuffix);
				}
				else {
					return dictionary[prefix];
				}
			}
		}

		public void Save(JSONWriter stream) {
			stream.CurvedOpen();
			stream.IncrementTabulation();
			stream.NewLine();

			var first = dictionary.First<KeyValuePair<string[], TSuffix>>();
			saveKeyValuePair(first, stream);

			foreach (var pair in dictionary.Skip<KeyValuePair<string[], TSuffix>>(1)) {
				stream.Comma();
				stream.NewLine();
				saveKeyValuePair(pair, stream);
			}

			stream.DecrementTabulation();
			stream.NewLine();
			stream.CurvedClose();
		}


		private SortedDictionary<string[], TSuffix> dictionary;

		private void saveKeyValuePair(KeyValuePair<string[], TSuffix> pair, JSONWriter stream) {
			stream.SquareOpen();
			stream.String(pair.Key[0]);
			for (int i = 1; i < pair.Key.Length; ++i) {
				stream.Comma();
				stream.Space();
				stream.String(pair.Key[i]);
			}
			stream.SquareClose();

			stream.Colon();

			pair.Value.Save(stream);
		}
	}
}
