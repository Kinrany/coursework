﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;

using Extensions;

using JSONStream;

namespace BS_Generator {

	abstract class Generator
	{
		public const int PREFIX_LENGTH = 2;

		public static Generator<SlimSuffix> Load(JSONReader stream) {
			stream.CurvedOpen();

			string languageModelString = stream.String();
			if (languageModelString != "languageModel")
				throw new InvalidDataException();

			stream.Colon();

			Generator<SlimSuffix> generator = new Generator<SlimSuffix>(LanguageModel.Load(stream));

			stream.CurvedClose();
			return generator;
		}
	}

	class Generator<TSuffix> 
		where TSuffix : ISuffixModel, new() 
	{
		public const int PREFIX_LENGTH = Generator.PREFIX_LENGTH;

		public Generator(TextReader reader = null) {
			languageModel = new LanguageModel<TSuffix>();

			if (reader != null) {
				Learn(reader);
			}
		}

		public Generator(LanguageModel<TSuffix> languageModel) {
			this.languageModel = languageModel;
		}

		public void Learn(TextReader reader) {
			StringEnumerator enumerator = new StringEnumerator(reader);
			enumerator.MoveNext();

			string[] prefix = new string[PREFIX_LENGTH];
			prefix.Fill("");

			string lastWord = enumerator.Current;
			languageModel.Add(prefix, lastWord);
			while (enumerator.MoveNext()) {
				prefix = prefix.Cycle(lastWord);
				lastWord = enumerator.Current;
				languageModel.Add(prefix, lastWord);
			}
		}

		public void Generate(TextWriter stream, int wordCount = 1000) {
			string[] start = new string[PREFIX_LENGTH];
			start.Fill("");
			Generate(stream, start, wordCount);
		}

		public void Generate(TextWriter stream, string[] start, int wordCount = 1000) {
			if (start == null) {
				throw new ArgumentNullException("start", "Задан пустой стартовый массив.");
			}
			if (start.Length != PREFIX_LENGTH) {
				throw new ArgumentException("Размер стартового массива не совпадает с установленным размером префикса.", "start");
			}

			string[] currentState = start;
			stream.Write(currentState.CustomToString());

			Random rng = new Random();

			int currentWordCount = 0;
			while (currentWordCount < wordCount) {
				TSuffix possibleSuffixes = languageModel[currentState];

				string suffix = rng.Pick<string>(possibleSuffixes);

				if (suffix == null) {
					Console.WriteLine("\n\nНе удалось сгенерировать текст заданной длины.");
					Console.WriteLine("Размер текста - {0} слов.", currentWordCount);
					break;
				}

				stream.Write("{0} ", suffix);
				currentWordCount++;

				currentState = currentState.Cycle(suffix);
			}
		}

		public void Save(JSONWriter stream) {
			stream.CurvedOpen();
			stream.String("languageModel");
			stream.Colon();
			languageModel.Save(stream);
			stream.CurvedClose();
		}


		// Хранит суффиксы для каждой введённой фразы заданной длины.
		private LanguageModel<TSuffix> languageModel;
	}
}
