﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Extensions;
using JSONStream;

namespace BS_Generator
{
	interface ISuffixModel : ICountable<string>
	{
		string GetRandom(Random rng);
		void Add(string word);
		void Save(JSONWriter stream);
	}

	/// <summary>
	/// Обеспечивает быстрое добавление новых суффиксов.
	/// </summary>
	class LazySuffix: List<string>, ISuffixModel
	{
		public string GetRandom(Random rng = null) {
			if (rng == null) {
				rng = new Random();
			}

			int index = rng.Next(this.Count);
			return this[index];
		}

		public SlimSuffix ToSlimSuffix() {
			this.Sort();
			return new SlimSuffix(this);
		}

		public void Save(JSONWriter stream) {
			ToSlimSuffix().Save(stream);
		}
	}

	/// <summary>
	/// Обеспечивает быстрое копирование и компактное хранение суффиксов.
	/// </summary>
	class SlimSuffix: ISuffixModel
	{
		public SlimSuffix() {
			sortedList = new SortedList<string, int>();
		}

		/// <summary>
		/// Создает модель из отсортированного списка.
		/// </summary>
		/// <param name="enumerable"></param>
		public SlimSuffix(IEnumerable<string> enumerable) {
			sortedList = new SortedList<string, int>();

			string currentWord = null;
			int count = 0;

			try {
				foreach (string word in enumerable) {
					if (word == currentWord) count++;
					else {
						if (count > 0) {
							sortedList.Add(currentWord, count);
						}

						currentWord = word;
						count = 1;
					}
				}
			}
			catch (ArgumentException e) {
				throw new ArgumentException("Строки не отсортированы.", "enumerable", e);
			}

			if (count > 0) {
				sortedList.Add(currentWord, count);
			}
		}


		public static SlimSuffix Load(JSONReader stream) {
			SlimSuffix slimSuffix = new SlimSuffix();

			stream.CurvedOpen();
			stream.SkipWhitespace();

			string key;
			int val;
			key = stream.String();
			stream.Colon();
			stream.Space();
			val = stream.Int();
			slimSuffix.sortedList.Add(key, val);

			while (stream.Peek() == ',') {
				stream.Comma();
				stream.SkipWhitespace();

				key = stream.String();
				stream.Colon();
				stream.Space();
				val = stream.Int();
				slimSuffix.sortedList.Add(key, val);
			}

			stream.SkipWhitespace();
			stream.CurvedClose();
			return slimSuffix;
		}


		#region ISuffixModel Members

		public string GetRandom(Random rng) {
			int index = rng.Next(Count);
			return this[index];
		}

		public void Add(string word) {
			int index = sortedList.IndexOfKey(word);
			if (index == -1) {
				sortedList.Add(word, 1);
			}
			else {
				sortedList[word] += 1;
			}

			PrefixSum = null;
		}

		public void Save(JSONWriter stream) {
			stream.CurvedOpen();
			stream.IncrementTabulation();
			stream.NewLine();

			var first = sortedList.First<KeyValuePair<string, int>>();
			stream.String(first.Key);
			stream.Colon();
			stream.Space();
			stream.Int(first.Value);

			foreach (var v in sortedList.Skip<KeyValuePair<string, int>>(1)) {
				stream.Comma();
				stream.NewLine();
				stream.String(v.Key);
				stream.Colon();
				stream.Space();
				stream.Int(v.Value);
			}

			stream.DecrementTabulation();
			stream.NewLine();
			stream.CurvedClose();
		}

		#endregion

		#region ICountable<string> Members

		/// <summary>
		/// Количество суффиксов в модели.
		/// </summary>
		public int Count {
			get {
				if (PrefixSum.Count > 0) {
					return PrefixSum[PrefixSum.Count - 1];
				}
				else {
					return 0;
				}
			}
		}

		/// <summary>
		/// Доступ к суффиксам по индексу.
		/// </summary>
		/// <param name="externalIndex">Индекс суффикса.</param>
		/// <returns>Суффикс в виде строки.</returns>
		public string this[int externalIndex] {
			get {
				if (externalIndex < 0 || externalIndex >= Count) {
					throw new ArgumentOutOfRangeException("externalIndex", "Нет суффикса с таким индексом.");
				}

				int internalIndex = PrefixSum.BinarySearch(externalIndex+1);
				if (internalIndex < 0) {
					internalIndex = ~internalIndex;
				}
				return sortedList.Keys[internalIndex];
			}
		}

		#endregion


		/// <summary>
		/// Пары (суффикс, количество упоминаний), отсортированные по суффиксу.
		/// </summary>
		private SortedList<string, int> sortedList;

		/// <summary>
		/// i-й элемент хранит сумму значений ячеек sortedList от 0-й до i-й.
		/// Последний элемент равен сумме всех значений в sortedList.
		/// </summary>
		private List<int> PrefixSum {
			get {
				if (prefixSum == null) {
					prefixSum = new List<int>(sortedList.Count);

					var values = sortedList.Values;

					prefixSum.Add(values[0]);
					for (int i = 1; i < values.Count; ++i) {
						prefixSum.Add(prefixSum[i-1] + values[i]);
					}
				}

				return prefixSum;
			}
			set {
				if (value != null)
					throw new InvalidOperationException();

				prefixSum = null;
			}
		}

		private List<int> prefixSum;
	}
}
