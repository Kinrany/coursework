﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

using Extensions;

namespace JSONStream
{
	interface IJSONCollection {
		void AddValue();
		void AddComma();
		void AddColon();
		void AddSquare();
		void AddCurved();
	}

	class JSONList: IJSONCollection
	{
		public enum ListState
		{
			Empty,
			Filled,
			Comma,
			Closed
		}

		public JSONList() {
			State = ListState.Empty;
		}

		public ListState State;

		public void AddValue() {
			if (State == ListState.Empty || State == ListState.Comma) {
				State = ListState.Filled;
			}
			else throw new InvalidOperationException();
		}

		public void AddComma() {
			if (State == ListState.Filled) {
				State = ListState.Comma;
			}
			else throw new InvalidOperationException();
		}

		public void AddColon() {
			throw new InvalidOperationException();
		}

		public void AddSquare() {
			if (State == ListState.Filled) {
				State = ListState.Closed;
			}
			else throw new InvalidOperationException();
		}

		public void AddCurved() {
			throw new InvalidOperationException();
		}
	}

	class JSONDictionary: IJSONCollection
	{
		public enum DictionaryState
		{
			Empty,
			Key,
			Colon,
			Value,
			Comma,
			Closed
		}

		public JSONDictionary() {
			State = DictionaryState.Empty;
		}

		public DictionaryState State;

		public void AddValue() {
			if (State == DictionaryState.Empty || State == DictionaryState.Comma) {
				State = DictionaryState.Key;
			}
			else if (State == DictionaryState.Colon) {
				State = DictionaryState.Value;
			}
			else throw new InvalidOperationException();
		}

		public void AddComma() {
			if (State == DictionaryState.Value) {
				State = DictionaryState.Comma;
			}
			else throw new InvalidOperationException();
		}

		public void AddColon() {
			if (State == DictionaryState.Key) {
				State = DictionaryState.Colon;
			}
			else throw new InvalidOperationException();
		}

		public void AddSquare() {
			throw new InvalidOperationException();
		}

		public void AddCurved() {
			if (State == DictionaryState.Value) {
				State = DictionaryState.Closed;
			}
			else throw new InvalidOperationException();
		}
	}


	class JSONWriter
	{
		public JSONWriter(TextWriter textWriter) {
			this.textWriter = textWriter;
			this.stack = new Stack<IJSONCollection>();
			this.stack.Push(new JSONList());
			tabulation = 0;
		}

		public void Close() {
			textWriter.Close();
		}

		public void CurvedOpen() {
			stack.Peek().AddValue();
			textWriter.Write('{');
			stack.Push(new JSONDictionary());
		}

		public void CurvedClose() {
			stack.Pop().AddCurved();
			textWriter.Write('}');
		}

		public void SquareOpen() {
			stack.Peek().AddValue();
			textWriter.Write('[');
			stack.Push(new JSONList());
		}

		public void SquareClose() {
			if (stack.Count == 1)
				throw new InvalidOperationException();

			stack.Pop().AddSquare();
			textWriter.Write(']');
		}

		public void Comma() {
			if (stack.Count == 1)
				throw new InvalidOperationException();

			stack.Peek().AddComma();
			textWriter.Write(',');
		}

		public void Colon() {
			stack.Peek().AddColon();
			textWriter.Write(':');
		}

		public void String(string str) {
			stack.Peek().AddValue();
			textWriter.Write('\"' + str.Replace("\"", "\"\"") + '\"');
		}

		public void Int(int num) {
			stack.Peek().AddValue();
			textWriter.Write(num);
		}

		public void NewLine() {
			textWriter.Write('\n');
			for (int i = 1; i <= tabulation; ++i) {
				textWriter.Write('\t');
			}
		}

		public void IncrementTabulation() {
			tabulation++;
		}

		public void DecrementTabulation() {
			if (tabulation == 0)
				throw new InvalidOperationException();

			tabulation--;
		}

		public void Space() {
			textWriter.Write(' ');
		}

		private TextWriter textWriter;
		private Stack<IJSONCollection> stack;

		private int tabulation;
	}

	class JSONReader
	{
		public JSONReader(TextReader textReader) {
			this.textReader = textReader;
			this.stack = new Stack<IJSONCollection>();
			this.stack.Push(new JSONList());
		}

		public void Close() {
			textReader.Close();
		}

		public void CurvedOpen() {
			stack.Peek().AddValue();
			textReader.RemoveCharacter('{');
			stack.Push(new JSONDictionary());
		}

		public void CurvedClose() {
			stack.Pop().AddCurved();
			textReader.RemoveCharacter('}');
		}

		public void SquareOpen() {
			stack.Peek().AddValue();
			textReader.RemoveCharacter('[');
			stack.Push(new JSONList());
		}

		public void SquareClose() {
			if (stack.Count == 1)
				throw new InvalidOperationException();

			stack.Pop().AddSquare();
			textReader.RemoveCharacter(']');
		}

		public void Comma() {
			if (stack.Count == 1)
				throw new InvalidOperationException();

			stack.Peek().AddComma();
			textReader.RemoveCharacter(',');
		}

		public void Colon() {
			stack.Peek().AddColon();
			textReader.RemoveCharacter(':');
		}

		public string String() {
			stack.Peek().AddValue();
			textReader.RemoveCharacter('\"');

			StringBuilder stringBuilder = new StringBuilder();
			while (true) {
				while (textReader.Peek() != '\"' && textReader.Peek() != -1) {
					stringBuilder.Append((char)textReader.Read());
				}

				textReader.RemoveCharacter('\"');
				if (textReader.Peek() != '\"') {
					break;
				}
				else {
					textReader.RemoveCharacter('\"');
					stringBuilder.Append('\"');
				}
			}

			return stringBuilder.ToString();
		}

		public int Int() {
			if (!char.IsDigit((char)textReader.Peek()))
				throw new InvalidOperationException();

			stack.Peek().AddValue();

			StringBuilder stringBuilder = new StringBuilder();
			while (textReader.Peek() >= (int)'0' && textReader.Peek() <= (int)'9') {
				stringBuilder.Append((char)textReader.Read());
			}

			return int.Parse(stringBuilder.ToString());
		}

		public int Peek() {
			return textReader.Peek();
		}

		public void SkipWhitespace() {
			while (char.IsWhiteSpace((char)textReader.Peek())) {
				textReader.Read();
			}
		}

		public void Space() {
			textReader.RemoveCharacter(' ');
		}

		private TextReader textReader;
		private Stack<IJSONCollection> stack;
	}
}
